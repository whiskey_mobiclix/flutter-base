import 'package:flutter/material.dart';
import 'package:flutterbase/configs/environment.config.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/i18n/i18n.dart';
import 'package:flutterbase/providers/general.provider.dart';
import 'package:flutterbase/core/routes/routes.dart';

import 'core/dependencies/dependenciesInjection.dart';

void main() async {
  await injection();
  getIt<Environment>().set("staging");
  runApp(App());
}

class App extends StatelessWidget {
  Widget build(BuildContext context) {
    I18n.set(getIt<GeneralProvider>().lang.value);

    return ValueListenableBuilder(
      valueListenable: getIt<GeneralProvider>().lang,
      builder: (ctx, lang, _) {
        return MaterialApp(
          debugShowCheckedModeBanner: true,
          routes: getIt<Router>().routes,
          builder: (context, child) {
            SizeConfig().init(context);
            return child;
          },
        );
      },
    );
  }
}
