import 'package:flutterbase/configs/environment.config.dart';
import 'package:flutterbase/configs/general.config.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/i18n/i18n.dart';
import 'package:flutterbase/providers/general.provider.dart';
import 'package:flutterbase/core/routes/routes.dart';
import 'package:get_it/get_it.dart';

final getIt = GetIt.instance;

Future<void> injection() async {
  //Router
  getIt.registerSingleton<Router>(Router());

  //Configs
  getIt.registerSingleton<GeneralConfig>(GeneralConfig());

  //Environment
  getIt.registerSingleton<Environment>(Environment());

  //Providers
  getIt.registerSingleton<GeneralProvider>(GeneralProvider());
}
