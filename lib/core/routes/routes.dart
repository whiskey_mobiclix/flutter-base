import 'package:flutter/cupertino.dart';
import 'package:flutterbase/screens/home.dart';

class Router {
  final Map<String, Widget Function(BuildContext)> routes = {
    "/": (_) => HomeScreen(),
  };

  Router();
}
