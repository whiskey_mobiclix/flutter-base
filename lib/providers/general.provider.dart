import 'package:flutter/cupertino.dart';
import 'package:flutterbase/core/dependencies/dependenciesInjection.dart';
import 'package:flutterbase/core/i18n/i18n.dart';

class GeneralProvider {
  ValueNotifier lang = ValueNotifier<String>("");

  void setLanguage(String lang) {
    I18n.set(lang);
    this.lang.value = lang;
  }
}
