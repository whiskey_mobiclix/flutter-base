class Environment {
  String mode = "staging";
  AppEnvironment data = new AppEnvironment();

  void set(String md) {
    this.mode = md;

    switch(md){
      case "staging":
        this.data = new AppEnvironment(
          apiUrl: "staging.api.com",
        );
        break;
      default:
        break;
    }
  }

  String get apiUrl{
    return this.data.apiUrl;
  }

  Environment();
}

class AppEnvironment {
  final apiUrl;

  AppEnvironment({this.apiUrl = "production.api.com"});
}
