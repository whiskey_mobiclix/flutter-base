import 'package:flutter/material.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/@services/services/user.service.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';

class HomeScreen extends StatelessWidget {
  Widget build(BuildContext context) {
    print(SizeConfig.screenWidth);

    return Container(
      color: Colors.red,
      child: FlatButton(
        child: CustomText(
          "Get user details",
        ),
        onPressed: () async {
          final user = await UserService.getDetailsById(id: "me");
          print(user.country.name);
        },
      ),
    );
  }
}
